from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.template import loader
from django.shortcuts import render, get_object_or_404

from haystack.views import SearchView
from rajdoot.models import Document


def docu_query(request, docu_id, query=""):
    print("Running docu_query")
    docu = get_object_or_404(Document, pk=docu_id)
    return render(request, 'rajdoot/docu.html', {'query': query, 'docu':
        docu})


class RajdootSearchView(SearchView):
    template = 'rajdoot/search-site.html'
