from bs4 import BeautifulSoup
import requests
import time
import os
import urllib.parse as urlparse
import datetime
from dateutil.parser import parse

SEARCH_URL = "http://odisha.gov.in/govtpress/gazdatewise.htm"

RESULT_URL = "http://odisha.gov.in/govtpress/gazdtsearch.asp"

req_args = {
    'timeout': 20,
    'headers': {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36',
        'Referer': SEARCH_URL,
    },
    'verify': False
}

def get_data_from_response(response_text):
    # Credit https://stackoverflow.com/a/23377804/1780891
    base_url = "http://odisha.gov.in/govtpress"
    soup = BeautifulSoup(response_text, 'html.parser')

    if "No record Found" in response_text:
        return []

    table_body = soup.find('table')

    rows = table_body.find_all('tr')
    data = list()
    col_keys = ["Department", "Notification Number", "Pub Date", "Gazette Number", "Subject", "url"]
    # First row is row header
    for row in rows[1:]:
        col_values = list()
        for col in row.find_all('td'):
            if col.text == "View":
                col_values.append("{}/{}".format(base_url, col.find('a')['href']))
            else:
                col_values.append(col.text)

        cols_dict = dict(zip(col_keys, col_values))
        cols_dict['Pub Date'] = parse(cols_dict['Pub Date'])
        data.append(cols_dict)

    return data


def poll_date(date):
    data = {
        'select': str(date.day).zfill(2),
        'select2': str(date.month).zfill(2),
        'select3': str(date.year)[-2:],
        'bsubmit': 'Submit'
            }

    s = requests.Session()
    r = s.post(RESULT_URL, data=data, **req_args)

    if r.ok:
        return get_data_from_response(r.text)
    else:
        r.raise_for_status()


def poll_from_date(start_date, end_date=datetime.date.today()):
    delta = datetime.timedelta(days=1)
    data = list()
    date = start_date
    while date <= end_date:
        data.extend(poll_date(date))
        date = date + delta

    return data
