from django import template
from django.utils.safestring import mark_safe
# import datetime

register = template.Library()


@register.simple_tag
def convert_url(format_string, prefix):
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(format_string, 'html.parser')
    for img in soup.findAll('img'):
        img['src'] = prefix + img['src']

    return mark_safe(str(soup))
