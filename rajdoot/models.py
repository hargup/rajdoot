from django.db import models


class Document(models.Model):
    pub_date = models.DateTimeField()
    title = models.CharField(max_length=200)
    doc_type = models.CharField(max_length=200, null=True)
    issued_by = models.CharField(max_length=200, null=True)
    body = models.TextField()
    html = models.TextField(null=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title
