from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from rajdoot.models import Document
from django.utils import timezone
from django.core.management import call_command
from rajdoot.settings import PROJECT_DIR
import os
import haystack


class DocumentMethodTests(TestCase):

    def test_dummy(self):
        self.assertIs(True, True)


class DocumentViewTests(TestCase):
    def test_document_no_query(self):
        d = Document(title="testing", body="testing testing testing",
                     pub_date=timezone.now(),
                     html="<html><p>testing testing testing</p></html>")
        d.save()

        response = self.client.get(reverse('docu', args=[d.id]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "testing")

    def test_document_with_query(self):
        """
        If no questions exist, an appropriate message should be displayed.
        """
        d = Document(title="testing", body="apple mango orange",
                     pub_date=timezone.now(),
                     html="<html><p>apple mango orange</p></html>")
        d.save()

        response = self.client.get(reverse('docu_query', args=[d.id, "mango"]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<span class=\"highlighted\">mango</span>")


TEST_INDEX = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_DIR, 'test_index'),
        'TIMEOUT': 60 * 10,
        'INDEX_NAME': 'test_index',
    },
}


@override_settings(HAYSTACK_CONNECTIONS=TEST_INDEX)
class SearchViewTests(TestCase):
    # Credit: http://bwreilly.github.io/blog/2013/07/21/testing-search-haystack-in-django/
    def setUp(self):
        haystack.connections.reload('default')
        d = Document(title="testing", body="apple mango orange",
                     pub_date=timezone.now(),
                     html="<html><p>apple mango orange</p></html>")
        d.save()
        call_command('update_index', interactive=False, verbosity=0)

    def test_search_view_no_query(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # There should be a form to submit the query
        self.assertContains(response, "form")

    def test_search_view_with_valid_query(self):
        response = self.client.get('/?q=mango')
        self.assertEqual(response.status_code, 200)
        # A query with result should have link to the result
        self.assertContains(response, "href")

    def test_search_view_with_gibrish_query(self):
        response = self.client.get('/?q=gibrish')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No results found")

    def tearDown(self):
        call_command('clear_index', interactive=False, verbosity=0)
