import os
import shutil
import glob
import internetarchive as ia
import subprocess
import shlex
from django.contrib.auth.models import User
from django.utils import timezone
from rajdoot.models import Document
from rajdoot.settings import STATICFILES_DIRS


def add_from_dir(base_dir):
    for pdf_name in os.listdir(base_dir):
        if not pdf_name.endswith(".pdf"):
            continue
        base_name = pdf_name.split(".")[0]

        if Document.objects.filter(title=base_name).exists():
            continue

        try:
            # XXX: Reading file as binary is dirty hack to bypass locale issues
            # on docker.
            with open("{}/{}.txt".format(base_dir, base_name), 'rb') as fp:
                text = fp.read()
        except IOError:
            print("Couldn't find txt for %s" % base_name)
            continue

        html = ""
        # XXX: Reading file as binary is dirty hack to bypass locale issues
        # on docker.
        with open("{}/{}-html.html".format(base_dir, base_name), 'rb') as fp:
            html = fp.read()

        # XXX: I'm assuming all the images from pdf to html conversion are .png
        # and they follow <name>_<something>.png convention.
        for src in glob.iglob(os.path.join(base_dir, "{}*.png".format(base_name))):
            shutil.copy(src, STATICFILES_DIRS[0])

        n = Document(pub_date=timezone.now(), title=base_name, body=text, html=html)
        n.save()
        print("%s added" % base_name)


def extract_text_from_dir(_dir):
    os.chdir(_dir) 
    for pdf_name in os.listdir("."):
        if not pdf_name.endswith(".pdf"):
            continue
        subprocess.call(["pdftotext", pdf_name])

    os.chdir("..")
    
def extract_html_from_dir(_dir):
    os.chdir(_dir) 
    for pdf_name in os.listdir("."):
        if not pdf_name.endswith(".pdf"):
            continue
        cmd = "pdftohtml -c -s %s" % pdf_name
        subprocess.call(shlex.split(cmd))
    os.chdir("..")

def add_from_ia_collection(collection):
    coll = ia.search_items('collection:%s'%collection)

    num = 0

    for result in coll: #for all items in a collection
        num = num + 1 #item count
        itemid = result['identifier']
        print('Downloading: #' + str(num) + '\t' + itemid)

        ia.download(itemid, verbose=True)    

        extract_text_from_dir(itemid)
        extract_html_from_dir(itemid)
        add_from_dir(itemid)


        shutil.rmtree(itemid)



