pip install -r requirements.txt
python manage.py makemigrations rajdoot
python manage.py migrate rajdoot
mkdir rajdoot/static
echo "from rajdoot.utils import add_from_dir; add_from_dir(\"$1\")"| python manage.py shell
python manage.py update_index
