# Rajdoot

Rajdoot intends to be the _Google Scholar_ for all official communications by
the Indian Governments. As of now we are working to create search tool on
[gazettes](https://github.com/hargup/gazette-of-india). A gazette is an
official journal and any new law, rule or regulation is notified through it. In
the future we also intend to include consultation papers and court orders and
also method to subscribe for alert for any new communication matching a search
query.

## What's up with the name?

Rajdoot in hindi stands for official messenger. So our Rajdoot will be a
messenger to you for all government communications.

Rajdoot is also the name of a [bike by Yamaha](https://en.wikipedia.org/wiki/Rajdoot_350)

## Installation Instructions

To run Rajdoot, you would need python 3.4 or higher with pip installed. If you
don't have strong opinions about the method of python installation, use the
latest version of [Anaconda](https://www.continuum.io/downloads)

Then run the following commands in your terminal:


```
git clone https://gitlab.com/hargup/rajdoot
```
```
cd rajdoot
```

You need to download the test documents, the text and html files for them are
already extracted.
```
wget https://gitlab.com/hargup/files_/raw/master/test_files_with_html.tar.gz
```
```
tar -xvf test_files_with_html.tar.gz
```

All django apps needs to be provided with an app secret, replace "DUMMY_SECRET"
with some random string when you use this in production.
```
echo "DUMMY_SECRET" > SECRET
```

Run the install script, it downloads the dependencies, loads the test documents
in the database and builds search index.
```
bash install.sh test_files
```

Now just run the server, to see the website working
```
python manage.py runserver localhost:8000
```

You can go your browser and open `localhost:8000/search` to search for
keyword in 100 test documents we have downloaded. It gives links to documents
containing the word. Clicking on the link will open it with the keyword
highlighted.

## What to help?

You are welcome to take up
any of the open issue [issues](https://gitlab.com/hargup/rajdoot/issues),
beginner friendly issues are marked as "Easy Fix". If you are not familiar with django
it would help to go through the [django tutorial first](https://docs.djangoproject.com/en/1.11/intro/tutorial01/).

If you want make any suggestion you can open issue for that.

## LICENSE

The project is licensed under [GNU AGPL Version 3](./LICENSE). See the [TL;DR Legal with summary of the license](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).

## Code of Conduct

In order to provide open and welcoming enviornment to contributors, we have
adapted [a code of conduct](./code-of-conduct.md) from [Contributor
Covenant][homepage].


[homepage]: https://www.contributor-covenant.org
